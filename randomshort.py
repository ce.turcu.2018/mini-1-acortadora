#!/usr/bin/python3

import random
import socket
import urllib.parse
import shelve
import copy

import webapp


class RandomShort(webapp.webApp):
    myPort = 1238
    mySocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    mySocket.bind(('localhost', myPort))

    # Max number of connections

    mySocket.listen(5)
    # Declare and initialize content
    URLs = shelve.open('URLs')

    def parse(self, request):
        """Return the resource name (including /)"""
        print("Request " + request)
        metodo = request.split(' ', 2)[0]
        recurso = request.split(' ', 2)[1]
        cuerpo = ''
        body = request.split('\r\n\r\n', 1)[1][len('=url'):]
        if not body.startswith('&key='):
            body = urllib.parse.unquote_plus(body)
            if body.startswith('https://') or body.startswith('http://'):
                cuerpo = body
            else:
                cuerpo = urllib.parse.unquote_plus('https://' + body)
            ## buscar cuerpo a partir del primer \r\n\r\n
        return (metodo, recurso, cuerpo)

    def str_dict(self):
        lista = ""
        for Url in self.URLs:
            lista += (Url + " :  " + str(self.URLs[Url]) + " --------------- ")
        return lista

    def process(self, parsedRequest):
        """Process the relevant elements of the request.

        Finds the HTML text corresponding to the resource name,
        ignoring requests for resources not in the dictionary.
        """
        (metodo, resourceName, cuerpo) = parsedRequest
        if (metodo == "POST"):
            if cuerpo:
                key = copy.copy(cuerpo)
                key = key.split('&', 2)[1][len('=key'):]
                cuerpo = cuerpo.split('&', 2)[0]
                if key:
                    self.URLs['/' + str(key)] = cuerpo

        if (resourceName == '/'):
            lista = self.str_dict()
            httpCode = "200 OK"
            htmlBody = "<html><body>" + "<h1>Lista de URLs acortadas</h1>"
            htmlBody += lista + "<p>"
            htmlBody += 'Inserte URL a acortar y clave'
            htmlBody += """
                    <br/><br/>
                    <form action="" method = "POST"> 
                    URL:<input type="text" name="url" value="">
                    CLAVE:<input type="text" name="key" value="">
                    <input type="submit" value="Enviar">
                    </form>

                """


        elif resourceName in self.URLs.keys():
            nextUrl = self.URLs[resourceName]
            # HTML body of the page to serve
            httpCode = "302 Found"
            htmlBody = nextUrl

        else:
            httpCode = "404 Not Found"
            htmlBody = "<html><body>"
            htmlBody += """
                    <h1>404 Not Found. Recurso no disponible </h1>
                """
        htmlBody += "</body></html>"
        return (httpCode, htmlBody)


if __name__ == "__main__":
    testWebApp = RandomShort("localhost", 1234)
